package org.amicofragile.katas.bowling

import java.util.*

class Game {
    private val frames: MutableList<Frame> = mutableListOf()

    fun addFrame(frame: Frame) {
        frames.add(frame)
    }

    val score: Int
        get() = frames.foldIndexed(0) {
            i, acc, curr -> acc + scoreOf(curr, i + 1)
        }
//        get() {
//            var score: Int = 0
//            for((i, f) in frames.withIndex()) {
//                if(i < Frames) {
//                    score += (f.firstBall + f.secondBall.orZero())
//                    if(f.isSpare) {
//                        score += frames.getOrNull(i + 1)?.firstBall.orZero()
//                    } else if(f.isStrike) {
//                        val nextForm = frames.getOrNull(i + 1)
//                        score += nextForm?.firstBall.orZero()
//                        score += (nextForm?.secondBall ?: frames.getOrNull(i + 2)?.firstBall).orZero()
//                    }
//                }
//            }
//            return score
//        }

    private fun scoreOf(frame: Frame, next: Int): Int {
        if(next > Frames) {
            return 0
        }
        val additionalScoreForSpare : Int = if(frame.isSpare) scoreForSpare(next) else 0
        val additionalScoreForStrike : Int = if(frame.isStrike) scoreForStrike(next) else 0
        return frame.firstBall + frame?.secondBall.orZero() + additionalScoreForSpare + additionalScoreForStrike
    }

    private fun scoreForSpare(next: Int) : Int = frames.getOrNull(next)?.firstBall.orZero()

    private fun scoreForStrike(next: Int) : Int {
        val nextFrameOpt = Optional.ofNullable(frames.getOrNull(next))
        return nextFrameOpt.map {
          f -> f.firstBall + (f.secondBall ?: scoreForSpare(next + 1))
        }.orElse(0)
    }


    val completed: Boolean
        get() = frames.size == Frames

    companion object {
        const val Frames: Int = 10
    }
}

fun Int?.orZero() : Int {
    return this ?: 0
}
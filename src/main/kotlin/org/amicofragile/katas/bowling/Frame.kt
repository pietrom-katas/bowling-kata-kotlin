package org.amicofragile.katas.bowling

class Frame(val firstBall : Int, val secondBall: Int?) {
    val isStrike: Boolean
        get() = firstBall == Pins

    val isSpare: Boolean
        get() = (secondBall != null && firstBall + secondBall == Pins)

    companion object {
        const val Pins : Int = 10

        fun strike() = Frame(Pins, null)
    }
}
package org.amicofragile.katas.bowling.oop

class OpenFrame(override val firstBall:Int, val secondBall:Int) : Frame() {
    init {
        if(firstBall + secondBall >= Frame.Pins) {
            throw IllegalArgumentException("Can't create an OpenFrame with more than ${Frame.Pins - 1} pins")
        }
    }

    override fun score(): Int = firstBall + secondBall

    override fun additionalScoreForStrike() = score()
}
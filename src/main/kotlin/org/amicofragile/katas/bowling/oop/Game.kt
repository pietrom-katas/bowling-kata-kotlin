package org.amicofragile.katas.bowling.oop

class Game {
    private val frames: MutableList<Frame> = mutableListOf()
    private var first: Boolean = true
    private var previewRoll: Int = 0

    fun roll(pins : Int) {
        if(first && pins == Frame.Pins) {
            addFrame(StrikeFrame())
        } else if(first) {
            previewRoll = pins
            first = false
        } else if(!first) {
            if(pins + previewRoll == Frame.Pins) {
                addFrame(SpareFrame(previewRoll))
            } else {
                addFrame(OpenFrame(previewRoll, pins))
            }
            first = true
            previewRoll = 0
        }
    }

    fun addFrame(frame: Frame) {
        if(frames.count() > 0) {
            frames.last().next = frame
        }
        frames.add(frame)
    }

    val completed: Boolean
        get() = frames.size == Frames

    val score: Int
        get() = frames.filterIndexed { i, _ -> i < Frames }
                .fold(0) { acc, f -> acc + f.score() }

    companion object {
        const val Frames: Int = 10
    }
}
package org.amicofragile.katas.bowling.oop

import org.amicofragile.katas.bowling.orZero

class SpareFrame(override val firstBall: Int) : Frame() {
    override fun score() : Int = Frame.Pins + next?.firstBall.orZero()

    override fun additionalScoreForStrike() = Frame.Pins
}
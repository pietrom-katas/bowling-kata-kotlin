package org.amicofragile.katas.bowling.oop

import org.amicofragile.katas.bowling.orZero

class StrikeFrame : Frame() {
    override fun score(): Int = Frame.Pins + next?.additionalScoreForStrike().orZero()

    override val firstBall: Int = Frame.Pins

    override fun additionalScoreForStrike() = Frame.Pins + next?.firstBall.orZero()
}
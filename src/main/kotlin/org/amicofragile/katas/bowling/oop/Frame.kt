package org.amicofragile.katas.bowling.oop

abstract class Frame {
    companion object {
        const val Pins: Int = 10
    }

    var next: Frame? = null

    abstract val firstBall: Int

    abstract fun score(): Int

    abstract fun additionalScoreForStrike(): Int
}
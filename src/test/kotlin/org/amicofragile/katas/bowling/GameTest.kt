package org.amicofragile.katas.bowling

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class GameTest {
    private val game: Game = Game()

    @Test
    fun initialScoreIsZero() {
        assertThat(game.score, `is`(equalTo(0)))
    }

    @Test
    fun singleFrameScore() {
        game.addFrame(Frame(5, 4))
        assertThat(game.score, `is`(equalTo(9)))
    }

    @Test
    fun multipleOpenFramesScore() {
        game.addFrame(Frame(5, 4))
        game.addFrame(Frame(5, 3))
        game.addFrame(Frame(5, 4))
        game.addFrame(Frame(5, 2))
        game.addFrame(Frame(1, 1))
        game.addFrame(Frame(5, 4))
        game.addFrame(Frame(5, 3))
        game.addFrame(Frame(5, 4))
        game.addFrame(Frame(5, 2))
        game.addFrame(Frame(1, 1))
        assertThat(game.score, `is`(equalTo(70)))
    }

    @Test
    fun completedGame() {
        game.addFrame(Frame(5, 4))
        game.addFrame(Frame(5, 3))
        game.addFrame(Frame(5, 4))
        game.addFrame(Frame(5, 2))
        game.addFrame(Frame(1, 1))
        game.addFrame(Frame(5, 4))
        game.addFrame(Frame(5, 3))
        game.addFrame(Frame(5, 4))
        game.addFrame(Frame(5, 2))
        game.addFrame(Frame(1, 1))
        assertThat(game.completed, `is`(true))
    }

    @Test
    fun incompleteGame() {
        game.addFrame(Frame(5, 4))
        game.addFrame(Frame(5, 3))
        game.addFrame(Frame(5, 4))
        assertThat(game.completed, `is`(false))
    }

    @Test
    fun incompleteGameWithSpareScore() {
        game.addFrame(Frame(6, 4))
        game.addFrame(Frame(5, 3))
        assertThat(game.score, `is`(equalTo(23)))
    }

    @Test
    fun spareScoreWithoutNext() {
        game.addFrame(Frame(5, 3))
        game.addFrame(Frame(5, 3))
        game.addFrame(Frame(6, 4))
        assertThat(game.score, `is`(equalTo(26)))
    }

    @Test
    fun incompleteGameWithMultipleSparesScore() {
        game.addFrame(Frame(7, 3))
        game.addFrame(Frame(5, 3))
        game.addFrame(Frame(7, 3))
        game.addFrame(Frame(5, 3))
        assertThat(game.score, `is`(equalTo(46)))
    }

    @Test
    fun incompleteGameWithStrikeScore() {
        game.addFrame(Frame.strike())
        game.addFrame(Frame(5, 3))
        assertThat(game.score, `is`(equalTo(26)))
    }

    @Test
    fun strikeScoreWithoutNext() {
        game.addFrame(Frame(5, 3))
        game.addFrame(Frame(5, 3))
        game.addFrame(Frame.strike())
        assertThat(game.score, `is`(equalTo(26)))
    }

    @Test
    fun incompleteGameWithMultipleStrikesScore() {
        game.addFrame(Frame.strike())
        game.addFrame(Frame(5, 3))
        game.addFrame(Frame.strike())
        game.addFrame(Frame(5, 3))
        assertThat(game.score, `is`(equalTo(52)))
    }

    @Test
    fun completeGameCloseToTheMaximumScore() {
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame(5, 4))
        assertThat(game.score, `is`(equalTo(263)))
    }

    @Test
    fun completeGameWithMaximumScore() {
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())

        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        assertThat(game.score, `is`(equalTo(300)))
    }

    @Test
    fun anotherVeryGoodCompleteGameScore() {
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame(9, 1))
        assertThat(game.score, `is`(equalTo(269)))
    }

    @Test
    fun anotherVeryGoodCompleteGame2Score() {
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())
        game.addFrame(Frame.strike())

        game.addFrame(Frame.strike())
        game.addFrame(Frame(5, null))

        assertThat(game.score, `is`(equalTo(295)))
    }
}
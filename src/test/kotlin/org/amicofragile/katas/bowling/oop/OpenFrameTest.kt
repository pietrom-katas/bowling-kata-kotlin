package org.amicofragile.katas.bowling.oop

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.IllegalArgumentException

class OpenFrameTest {
    @Test
    fun calculateScore() {
        assertThat(OpenFrame(7, 2).score(), `is`(equalTo(9)))
    }

    @Test
    fun cantCreateOpenFrameWithMoreThanNinePins() {
        assertThrows<IllegalArgumentException> {
            val frame: Frame = OpenFrame(7, 7)
        }
    }

    @Test
    fun cantCreateOpenFrameWithMoreThanNinePinsAsFirstBall() {
        assertThrows<IllegalArgumentException> {
            val frame: Frame = OpenFrame(10, 0)
        }
    }
}
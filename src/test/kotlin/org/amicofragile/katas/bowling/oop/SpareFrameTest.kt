package org.amicofragile.katas.bowling.oop

import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class SpareFrameTest {
    @Test
    fun calculateScoreWithoutNext() {
        assertThat(SpareFrame(7).score(), CoreMatchers.`is`(CoreMatchers.equalTo(10)))
    }

    @Test
    fun calculateScoreWithNext() {
        val frame = SpareFrame(7)
        frame.next = OpenFrame(8, 1)
        assertThat(frame.score(), CoreMatchers.`is`(CoreMatchers.equalTo(18)))
    }
}
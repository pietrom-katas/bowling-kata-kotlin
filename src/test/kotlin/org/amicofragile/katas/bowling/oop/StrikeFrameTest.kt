package org.amicofragile.katas.bowling.oop

import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class StrikeFrameTest {
    @Test
    fun calculateScoreWithoutNext() {
        assertThat(StrikeFrame().score(), CoreMatchers.`is`(CoreMatchers.equalTo(10)))
    }

    @Test
    fun calculateScoreWithNextOpenFrame() {
        val frame = StrikeFrame()
        frame.next = OpenFrame(8, 1)
        assertThat(frame.score(), CoreMatchers.`is`(CoreMatchers.equalTo(19)))
    }

    @Test
    fun calculateScoreWithNextStrike() {
        val frame = StrikeFrame()
        val next = StrikeFrame()
        frame.next = next
        val third = OpenFrame(8, 0)
        next.next = third
        assertThat(frame.score(), CoreMatchers.`is`(CoreMatchers.equalTo(28)))
    }
}
package org.amicofragile.katas.bowling.oop

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test


class GameTest {
    private val game: Game = Game()

    @Test
    fun initialScoreIsZero() {
        assertThat(game.score, `is`(equalTo(0)))
    }

    @Test
    fun singleFrameScore() {
        game.addFrame(OpenFrame(5, 4))
        assertThat(game.score, `is`(equalTo(9)))
    }

    @Test
    fun multipleOpenFramesScore() {
        game.addFrame(OpenFrame(5, 4))
        game.addFrame(OpenFrame(5, 3))
        game.addFrame(OpenFrame(5, 4))
        game.addFrame(OpenFrame(5, 2))
        game.addFrame(OpenFrame(1, 1))
        game.addFrame(OpenFrame(5, 4))
        game.addFrame(OpenFrame(5, 3))
        game.addFrame(OpenFrame(5, 4))
        game.addFrame(OpenFrame(5, 2))
        game.addFrame(OpenFrame(1, 1))
        assertThat(game.score, `is`(equalTo(70)))
    }

    @Test
    fun multipleOpenFramesScoreAddingRollByRoll() {
        game.roll(5)
        game.roll(4)
        game.roll(5)
        game.roll(3)
        game.roll(5)
        game.roll(4)
        assertThat(game.score, `is`(equalTo(26)))
    }

    @Test
    fun completedGame() {
        game.addFrame(OpenFrame(5, 4))
        game.addFrame(OpenFrame(5, 3))
        game.addFrame(OpenFrame(5, 4))
        game.addFrame(OpenFrame(5, 2))
        game.addFrame(OpenFrame(1, 1))
        game.addFrame(OpenFrame(5, 4))
        game.addFrame(OpenFrame(5, 3))
        game.addFrame(OpenFrame(5, 4))
        game.addFrame(OpenFrame(5, 2))
        game.addFrame(OpenFrame(1, 1))
        assertThat(game.completed, `is`(true))
    }

    @Test
    fun incompleteGame() {
        game.addFrame(OpenFrame(5, 4))
        game.addFrame(OpenFrame(5, 3))
        game.addFrame(OpenFrame(5, 4))
        assertThat(game.completed, `is`(false))
    }

    @Test
    fun incompleteGameWithSpareScore() {
        game.addFrame(SpareFrame(6))
        game.addFrame(OpenFrame(5, 3))
        assertThat(game.score, `is`(equalTo(23)))
    }

    @Test
    fun incompleteGameWithSpareScoreAddingRollByRoll() {
        game.roll(6)
        game.roll(4)
        game.roll(5)
        game.roll(3)
        assertThat(game.score, `is`(equalTo(23)))
    }

    @Test
    fun spareScoreWithoutNext() {
        game.addFrame(OpenFrame(5, 3))
        game.addFrame(OpenFrame(5, 3))
        game.addFrame(SpareFrame(6))
        assertThat(game.score, `is`(equalTo(26)))
    }

    @Test
    fun incompleteGameWithMultipleSparesScore() {
        game.addFrame(SpareFrame(7))
        game.addFrame(OpenFrame(5, 3))
        game.addFrame(SpareFrame(7))
        game.addFrame(OpenFrame(5, 3))
        assertThat(game.score, `is`(equalTo(46)))
    }

    @Test
    fun incompleteGameWithStrikeScore() {
        game.addFrame(StrikeFrame())
        game.addFrame(OpenFrame(5, 3))
        assertThat(game.score, `is`(equalTo(26)))
    }

    @Test
    fun strikeScoreWithoutNext() {
        game.addFrame(OpenFrame(5, 3))
        game.addFrame(OpenFrame(5, 3))
        game.addFrame(StrikeFrame())
        assertThat(game.score, `is`(equalTo(26)))
    }

    @Test
    fun incompleteGameWithMultipleStrikesScore() {
        game.addFrame(StrikeFrame())
        game.addFrame(OpenFrame(5, 3))
        game.addFrame(StrikeFrame())
        game.addFrame(OpenFrame(5, 3))
        assertThat(game.score, `is`(equalTo(52)))
    }

    @Test
    fun completeGameCloseToTheMaximumScore() {
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(OpenFrame(5, 4))
        assertThat(game.score, `is`(equalTo(263)))
    }

    @Test
    fun completeGameWithMaximumScore() {
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())

        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        assertThat(game.score, `is`(equalTo(300)))
    }
    @Test
    fun completeGameWithMaximumScoreAddingRollByRoll() {
        game.roll(10)
        game.roll(10)
        game.roll(10)
        game.roll(10)
        game.roll(10)
        game.roll(10)
        game.roll(10)
        game.roll(10)
        game.roll(10)
        game.roll(10)

        game.roll(10)
        game.roll(10)
        assertThat(game.score, `is`(equalTo(300)))
    }

    @Test
    fun anotherVeryGoodCompleteGameScore() {
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(SpareFrame(9))
        assertThat(game.score, `is`(equalTo(269)))
    }

    @Test
    fun anotherVeryGoodCompleteGame2Score() {
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())
        game.addFrame(StrikeFrame())

        game.addFrame(StrikeFrame())
        game.addFrame(OpenFrame(5, 0))

        assertThat(game.score, `is`(equalTo(295)))
    }
}
package org.amicofragile.katas.bowling

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class FrameTest {
    @Test
    fun canTestStrike() {
        assertThat(Frame.strike().isStrike, `is`(true))
    }

    @Test
    fun canTestSpare() {
        assertThat(Frame(6, 4).isSpare, `is`(true))
    }

    @Test
    fun openFrameIsNotSpare() {
        assertThat(Frame(5, 4).isSpare, `is`(false))
    }

    @Test
    fun openFrameIsNotStrike() {
        assertThat(Frame(5, 4).isStrike, `is`(false))
    }

    @Test
    fun spareIsNotStrike() {
        assertThat(Frame(6, 4).isStrike, `is`(false))
    }
}